import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { style } from 'typestyle'
import { toggleHotkeyMenu } from '../store/actions'
import { AppState } from '../store/store'

const buttonHeight = 20
const panelHeight = 240

const containerClassName = style({
  flex: 'none',
  display: 'flex',
  flexDirection: 'column',
})

const toggleButtonClassName = style({
  flex: 'none',
  height: buttonHeight,
  fontSize: buttonHeight,
  borderTop: '1px solid lightgray',
  borderLeft: '1px solid lightgray',
  borderRight: '1px solid lightgray',
  borderTopLeftRadius: buttonHeight,
  borderTopRightRadius: buttonHeight,
  color: 'white',
  background: 'lightgray',
  textAlign: 'center',
  $nest: {
    '&:hover': {
      color: 'white',
      background: 'gray',
    },
  },
})

const toggleButtonOpenClassName = style({
  $nest: {
    '&::before': {
      content: `"\\f077"`
    }
  },
})

const toggleButtonCloseClassName = style({
  $nest: {
    '&::before': {
      content: `"\\f078"`
    }
  },
})

const menuClassName = style({
  flex: 'none',
  display: 'flex',
  flexDirection: 'column',
  flexWrap: 'wrap',
  alignContent: 'space-evenly',
  height: panelHeight,
  textAlign: 'center',
  color: 'gray',
  transition: 'margin-bottom 0.3s ease-in-out',
})

const menuOpenedClassName = style({
  marginBottom: 0,
})

const menuClosedClassName = style({
  marginBottom: -panelHeight,
})

const hotkeyContainerClassName = style({
  display: 'flex',
  height: '20%',
  alignItems: 'center',
  margin: 0,
})

const hotkeyClassName = style({
  flex: 'none',
  display: 'inline-block',
  width: 32,
  height: 32,
  lineHeight: '32px',
  fontSize: 20,
  border: '2px solid lightgray',
  borderRadius: 8,
})

const hotkeyDescriptionClassName = style({
  flex: 1,
  textAlign: 'left',
  paddingLeft: 8,
})

export default function() {
  const dispatch = useDispatch()
  const menuOpened = useSelector((state: AppState) => state.hotkeyMenuOpened)

  return (
    <div className={containerClassName}>
      <div className={`fas ${toggleButtonClassName} ${menuOpened ? toggleButtonCloseClassName : toggleButtonOpenClassName}`} onClick={() => dispatch(toggleHotkeyMenu())} />
      <div className={`${menuClassName} ${menuOpened ? menuOpenedClassName : menuClosedClassName}`}>
        <pre className={hotkeyContainerClassName}>
          <span className={hotkeyClassName}>1</span>
          <span className={hotkeyDescriptionClassName}>
            <i className='fas fa-toggle-on' /> / <i className='fas fa-toggle-off' /> Tier 1 Heros
          </span>
        </pre>
        <pre className={hotkeyContainerClassName}>
          <span className={hotkeyClassName}>2</span>
          <span className={hotkeyDescriptionClassName}>
          <i className='fas fa-toggle-on' /> / <i className='fas fa-toggle-off' /> Tier 2 Heros
          </span>
        </pre>
        <pre className={hotkeyContainerClassName}>
          <span className={hotkeyClassName}>3</span>
          <span className={hotkeyDescriptionClassName}>
          <i className='fas fa-toggle-on' /> / <i className='fas fa-toggle-off' /> Tier 3 Heros
          </span>
        </pre>
        <pre className={hotkeyContainerClassName}>
          <span className={hotkeyClassName}>4</span>
          <span className={hotkeyDescriptionClassName}>
          <i className='fas fa-toggle-on' /> / <i className='fas fa-toggle-off' /> Tier 4 Heros
          </span>
        </pre>
        <pre className={hotkeyContainerClassName}>
          <span className={hotkeyClassName}>5</span>
          <span className={hotkeyDescriptionClassName}>
          <i className='fas fa-toggle-on' /> / <i className='fas fa-toggle-off' /> Tier 5 Heros
          </span>
        </pre>
        <pre className={hotkeyContainerClassName}>
          <span className={hotkeyClassName}>Q</span>
          <span className={hotkeyDescriptionClassName}>
            <i className='fas fa-lock' /> / <i className='fas fa-unlock' /> Hero Selection
          </span>
        </pre>
        <pre className={hotkeyContainerClassName}>
          <span className={hotkeyClassName}>W</span>
          <span className={hotkeyDescriptionClassName}>
            <i className='fas fa-trash' /> Hero Selection
          </span>
        </pre>
        <pre className={hotkeyContainerClassName}>
          <span className={hotkeyClassName}>E</span>
          <span className={hotkeyDescriptionClassName}>
            Rotate The Wheel <i className='fas fa-undo-alt' />
          </span>
        </pre>
        <pre className={hotkeyContainerClassName}>
          <span className={hotkeyClassName}>R</span>
          <span className={hotkeyDescriptionClassName}>
            Rotate The Wheel <i className='fas fa-redo-alt' />
          </span>
        </pre>
        <pre className={hotkeyContainerClassName}>
          <span className={hotkeyClassName}>&#9251;</span>
          <span className={hotkeyDescriptionClassName}>
            <i className='fas fa-chevron-up' /> / <i className='fas fa-chevron-down ' /> Hotkeys Menu
          </span>
        </pre>
      </div>
    </div>
  )
}