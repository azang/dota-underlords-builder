import React, { useRef, useEffect } from 'react'
import * as d3 from 'd3'
import Constant from '../utils/Constant'
import { tierColors } from '../utils/Scale'
import { Hero } from '../models/Hero'
import { useDispatch, useSelector } from 'react-redux'
import { setHoveredHero, toggleHeroSelection } from '../store/actions'
import {
  heroHiddenSelector,
  heroHoveredSelector,
  heroInHoveredAllianceSelector,
  heroSelectedSelector
} from '../store/selectors'
import { Arc, getArc, middleAngle, Sector } from '../utils/Shape'
import { ColorHelper } from 'csx'
import { AppState } from '../store/store'

type CHeroProps = {
  hero: Hero
  sector: Sector
}

export default function({hero, sector}: CHeroProps) {
  const ref = useRef<SVGGElement>(null)
  const dispatch = useDispatch()
  const tierIndex = hero.tier - 1
  const tierColor = tierColors(tierIndex) as ColorHelper
  const locked = useSelector((state: AppState) => state.heroSelectionLocked)
  const heroHovered = useSelector(heroHoveredSelector(hero))
  const heroInHoveredAlliance = useSelector(heroInHoveredAllianceSelector(hero))
  const heroSelected = useSelector(heroSelectedSelector(hero))
  const heroHidden = useSelector(heroHiddenSelector(hero))
  const highlight = heroHovered || heroInHoveredAlliance || heroSelected
  // draw the hero components
  useEffect(
    () => {
      const arc = getArc({ring: Constant.TIER_RINGS[tierIndex], sector, padding: 4} as Arc)
      const rotate = 180 * middleAngle(sector) / Math.PI
      if (ref.current) {
        const group = d3.select(ref.current)
        group.select('path.hero-arc')
          .attr('d', arc.path)
          .attr('pointer-events', locked ? 'none' : 'unset')
          .on('mouseenter', (_: MouseEvent) => dispatch(setHoveredHero(hero.id)))
          .on('mouseleave', (_: MouseEvent) => dispatch(setHoveredHero('')))
          .on('click', (_: MouseEvent) => dispatch(toggleHeroSelection(hero.id)))
        group.select('use.hero-icon')
          .attr('href', `#${hero.id}`)
          .attr('x', arc.center.x - Constant.HERO_ICON / 2)
          .attr('y', arc.center.y - Constant.HERO_ICON / 2)
          .attr('transform', `rotate(${rotate}, ${arc.center.x}, ${arc.center.y})`)
          .attr('pointer-events', 'none')
      }
    },
    [ref, hero, sector, tierIndex, locked, dispatch]
  )
  // highlight the hero arc
  useEffect(
    () => {
      if (ref.current) {
        d3.select(ref.current)
          .select('path.hero-arc')
          .transition()
          .duration(200)
          .attr('stroke', (heroSelected ? tierColor.darken(0.25) : tierColor).toHexString())
          .attr('stroke-width', heroSelected ? 4 : 2)
          .attr('fill', highlight ? tierColor.tint(0.25).toHexString() : 'transparent')
      }
    },
    [ref, tierColor, highlight, heroSelected]
  )
  // show/hide the hero
  useEffect(
    () => {
      if (ref.current) {
        d3.select(ref.current)
          .transition()
          .duration(200)
          .attr('display', heroHidden ? 'none' : 'unset')
      }
    },
    [ref, heroHidden]
  )

  return (
    <g className='hero' ref={ref}>
      <path className='hero-arc' />
      <use className='hero-icon' />
    </g>
  )
}