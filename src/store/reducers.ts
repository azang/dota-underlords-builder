import { PayloadAction, combineReducers, createReducer } from '@reduxjs/toolkit'
import {
  setHoveredAlliance,
  setHoveredHero,
  toggleHeroSelection,
  toggleTier,
  rotateWheel,
  lockUnlockHeroSelection,
  clearHeroSelection,
  togglePlayerLevel,
  toggleHotkeyMenu
} from './actions'
import { Alliance } from '../models/Alliance'
import { Alliances } from '../data/Alliances'
import { Hero } from '../models/Hero'
import { Heros } from '../data/Heros'
import Constant from '../utils/Constant'

const hoveredAlliance = createReducer<Alliance | null>(null, {
  [setHoveredAlliance.type]: (_, action: PayloadAction<string>) => Alliances.get(action.payload) ?? null
})

const hoveredHero = createReducer<Hero | null>(null, {
  [setHoveredHero.type]: (_, action: PayloadAction<string>) => Heros.get(action.payload) ?? null
})

const selectedHeros = createReducer<Set<Hero>>(new Set(), {
  [toggleHeroSelection.type]: (state: Set<Hero>, action: PayloadAction<string>) => {
    const hero = Heros.get(action.payload)!
    // can select at most 10 heros
    if (state.has(hero)) {
      state.delete(hero)
    } else if (state.size < 10) {
      state.add(hero)
    }
    return state
  },
  [clearHeroSelection.type]: () => new Set()
})

const hideTier1 = createReducer<boolean>(false, {
  [toggleTier.type]: (state: boolean, action: PayloadAction<number>) =>
    action.payload === 1 ? !state : state
})

const hideTier2 = createReducer<boolean>(false, {
  [toggleTier.type]: (state: boolean, action: PayloadAction<number>) =>
    action.payload === 2 ? !state : state
})

const hideTier3 = createReducer<boolean>(false, {
  [toggleTier.type]: (state: boolean, action: PayloadAction<number>) =>
    action.payload === 3 ? !state : state
})

const hideTier4 = createReducer<boolean>(false, {
  [toggleTier.type]: (state: boolean, action: PayloadAction<number>) =>
    action.payload === 4 ? !state : state
})

const hideTier5 = createReducer<boolean>(false, {
  [toggleTier.type]: (state: boolean, action: PayloadAction<number>) =>
    action.payload === 5 ? !state : state
})

const wheelRotateOffset = createReducer<number>(0, {
  [rotateWheel.type]: (state: number, action: PayloadAction<1|-1>) =>
    (state + action.payload) % Constant.ALLIANCE_COUNT
})

const heroSelectionLocked = createReducer<boolean>(false, {
  [lockUnlockHeroSelection.type]: (state: boolean) => !state
})

const hotkeyMenuOpened = createReducer<boolean>(false, {
  [toggleHotkeyMenu.type]: (state: boolean) => !state
})

const playerLevel = createReducer<number>(1, {
  [togglePlayerLevel.type]: (state: number, action: PayloadAction<1|-1>) =>
    state + action.payload
})

export default combineReducers({
  hoveredAlliance,
  hoveredHero,
  selectedHeros,
  hideTier1,
  hideTier2,
  hideTier3,
  hideTier4,
  hideTier5,
  wheelRotateOffset,
  heroSelectionLocked,
  hotkeyMenuOpened,
  playerLevel,
})
