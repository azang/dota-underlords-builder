import React from 'react'
import { style } from 'typestyle'
import Constant from '../utils/Constant'

const footerClassName = style({
  flex: 'none',
  display: 'flex',
  fontSize: 12,
  padding: 8,
  borderTop: '1px solid lightgray',
  alignItems: 'center',
  justifyContent: 'space-between',
  background: 'white',
  $nest: {
    'a': {
      $nest: {
        '&:hover': {
          textDecoration: 'underline',
        },
      },
      textDecoration: 'none',
    },
    '.icons': {
      fontSize: 0,
    }
  }
})

export default function() {
  return (
    <div className={footerClassName}>
      <div className='icons'>
        <a href='https://www.underlords.com/' target='_blank' rel='noopener noreferrer'>
          <img src='dota-underlords-logo.svg' alt='Official Site' height='20px' />
        </a>
        <a href='https://gitlab.com/azang/dota-underlords-builder' target='_blank' rel='noopener noreferrer'>
          <img src='gitlab-logo.svg' alt='Hosted on GitLab Pages' height='20px' />
        </a>
      </div>
      <div>Updated to <a href='https://www.underlords.com/updates' target='_blank' rel='noopener noreferrer'>{Constant.UPDATE_DATE}</a></div>
    </div>
  )
}