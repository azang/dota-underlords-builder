import { Alliance } from '../models/Alliance'
import { Bonus } from '../models/Bonus'

function tag(strings: TemplateStringsArray, ...keys: number[]): (...values: string[]) => string {
  return (function(...values: string[]): string {
    // strings is always having one more element then values
    const result = [strings[0]]
    values.forEach((value, index) => result.push(value, strings[index + 1]))
    return result.join('')
  })
}

const Assassin = new Alliance(
  'Assassin',
  '#462d52',
  ['Bounty_hunter', 'Ember_spirit', 'Faceless_void', 'Phantom_assassin', 'Queen_of_pain', 'Slark', 'Templar_assassin'],
  new Bonus(
    3,
    2,
    [['15', '325', '75'], ['25', '400', '100']],
    tag`All Assassins gain a ${0}% chance to Critical Hit for ${1}% Damage. On a crit, the target is WOUNDED and healing taken is reduced by ${2}% for 3 seconds.`
  ),
)
const Brawny = new Alliance(
  'Brawny',
  '#961010',
  ['Snapfire', 'Juggernaut', 'Bristleback', 'Beastmaster', 'Axe'],
  new Bonus(
    2,
    2,
    [['5'], ['10']],
    tag`All Brawny units gain +${0} maximum health for each kill by any of your team Brawny units throughout the game. Bonus is multiplied by star level of Brawny unit. Summons count as 0.25 kill. (HP bonus varies by game mode).`
  ),
)
const Brute = new Alliance(
  'Brute',
  '#645447',
  ['Spirit_breaker', 'Alchemist', 'Lifestealer', 'Doom', 'Axe'],
  new Bonus(
    2,
    2,
    [['30', '80'], ['60', '150']],
    tag`Whenever a Brute attacks an enemy it applies a -${0}% damage debuff for 4 seconds. When the debuff is initially applied it deals ${1} damage. Brutes prefer to attack enemies that do not have the debuff.`
  ),
)
const Champion = new Alliance(
  'Champion',
  '#8065a6',
  ['Legion_commander'],
  new Bonus(
    1,
    1,
    [[]],
    tag`Champions receive all Alliance bonuses. In addition, they gain 3% extra damage and 8% max health per active Alliance.`
  ),
)
const Demon = new Alliance(
  'Demon',
  '#81115e',
  ['Shadow_demon', 'Queen_of_pain', 'Chaos_knight', 'Terrorblade', 'Spectre', 'Doom'],
  new Bonus(
    1,
    1,
    [[]],
    tag`Whenever a Demon casts a spell, for 4 seconds allied Demons are silenced and the caster gains +50% bonus Pure damage for each allied Demon.`
  ),
)
const Dragon = new Alliance(
  'Dragon',
  '#c23809',
  ['Snapfire', 'Puck', 'Viper', 'Dragon_knight'],
  new Bonus(
    2, 1,
    [[]],
    tag`All dragon units unlock an additional draconic ability.`
  ),
)
const Fallen = new Alliance(
  'Fallen',
  '#cc62be',
  ['Lich', 'Vengeful_spirit', 'Abaddon', 'Terrorblade', 'Death_prophet', 'Wraith_king'],
  new Bonus(
    3,
    2,
    [['0'], ['1']],
    tag`When each Fallen unit first dies, allies within 2 cells are healed for 250 HP and gain +10% bonus damage to attacks and spells. A Fallen unit can revive ${0} times per combat, resurrecting with 40% health.`
  ),
)
const Healer = new Alliance(
  'Healer',
  '#38a14a',
  ['Enchantress', 'Dazzle', 'Treant_protector', 'Omniknight'],
  new Bonus(
    2,
    2,
    [['25'], ['45']],
    tag`All friendly healing is amplified by ${0}%.`
  ),
)
const Heartless = new Alliance(
  'Heartless',
  '#5e7773',
  ['Drow_ranger', 'Shadow_demon', 'Vengeful_spirit', 'Pudge', 'Lifestealer', 'Death_prophet'],
  new Bonus(
    2,
    3,

    [['4'], ['9'], ['15']],
    tag`All Enemies lose ${0} Armor.`
  ),
)
const Human = new Alliance(
  'Human',
  '#00a17d',
  ['Crystal_maiden', 'Kunkka', 'Legion_commander', 'Lycan', 'Omniknight', 'Lina', 'Keeper_of_the_light', 'Dragon_knight'],
  new Bonus(
    2,
    3,
    [['4'], ['8'], ['15']],
    tag`Allies generate ${0} mana per second.`
  ),
)
const Hunter = new Alliance(
  'Hunter',
  '#de6644',
  ['Drow_ranger', 'Anti-mage', 'Windranger', 'Beastmaster', 'Terrorblade', 'Mirana', 'Medusa'],
  new Bonus(
    3,
    2,
    [['30', '2'], ['38', '3']],
    tag`All Hunters have a ${0}% chance of quickly performing ${1} attacks.`
  ),
)
const Knight = new Alliance(
  'Knight',
  '#d7c019',
  ['Batrider', 'Chaos_knight', 'Luna', 'Omniknight', 'Abaddon', 'Sven', 'Dragon_knight'],
  new Bonus(
    2,
    3,
    [['15', '15'], ['20', '20'], ['25', '25']],
    tag`Knight units take ${0}% less physical and magical damage. If attacked from the front, and within 1 cell of an allied Knight, Knight units take an additional ${1}% less physical and magical damage.`
  ),
)
const Mage = new Alliance(
  'Mage',
  '#3ea6af',
  ['Crystal_maiden', 'Lich', 'Storm_spirit', 'Puck', 'Lina', 'Rubick', 'Keeper_of_the_light'],
  new Bonus(
    3,
    2,
    [['35'], ['70']],
    tag`Enemies take ${0}% more Magic damage.`
  ),
)
const Magus = new Alliance(
  'Magus',
  '#3d56d6',
  ['Rubick'],
  new Bonus(
    1,
    1,
    [[]],
    tag`Once per battle, when a Magus unit's health drops below 50%, the unit becomes invulnerable and unable to attack for 3 seconds.`
  ),
)
const Poisoner = new Alliance(
  'Poisoner',
  '#006e6e',
  ['Venomancer', 'Dazzle', 'Queen_of_pain', 'Alchemist', 'Viper'],
  new Bonus(
    3,
    1,
    [[]],
    tag`Attacks from Poisoners apply stacks of Poison. Enemies lose 7 Attack Speed per stack of Poison.`
  ),
)
const Rogue = new Alliance(
  'Rogue',
  '#8a1512',
  ['Bounty_hunter', 'Anti-mage', 'Meepo', 'Alchemist', 'Sven'],
  new Bonus(
    2,
    2,
    [['20', '10'], ['30', '20']],
    tag`Allied Rogue units gain ${0}% evasion. Whenever a Rogue unit evades an attack, it gains +${1} Attack Damage.`
  ),
)
const Savage = new Alliance(
  'Savage',
  '#99330d',
  ['Tusk', 'Magnus', 'Bristleback', 'Spirit_breaker', 'Lycan', 'Lone_druid', 'Pangolier'],
  new Bonus(
    2,
    3,
    [['Allied Savage units', '5'], ['Allied Savage units and all allied summons', '8'], ['All allied units', '13']],
    tag`${0} gain +${1} Attack Damage each time they land an attack.`
  ),
)
const Scaled = new Alliance(
  'Scaled',
  '#30297a',
  ['Slardar', 'Venomancer', 'Slark', 'Tidehunter', 'Medusa'],
  new Bonus(
    2,
    2,
    [['20'], ['50']],
    tag`Allies gain +${0}% Magic Resistance.`
  ),
)
const Shaman = new Alliance(
  'Shaman',
  '#1d4d2e',
  ['Enchantress', 'Magnus', 'Nature\'s_prophet', 'Beastmaster', 'Treant_protector', 'Lone_druid'],
  new Bonus(
    2,
    3,
    [['Small Wolf'], ['Wildwing Hatchling'], ['Black Dragon']],
    tag`When combat starts, a ${0} is summoned to help.`
  ),
)
const Spirit = new Alliance(
  'Spirit',
  '#464c7d',
  ['Storm_spirit', 'Earth_spirit', 'Ember_spirit', 'Void_spirit'],
  new Bonus(
    3,
    1,
    [[]],
    tag`When a Spirit casts an ability they enter a charged state. When 3 Spirits are in a charged state they perform the forbidden technique of the triangular Delta Slam, each dealing 130 magic damage to all enemies caught inside and applying their elemental effects for 1.5 seconds. Damage and effect duration are multiplied by 1.5 at 2* and 2 at 3*.`
  ),
)
const Summoner = new Alliance(
  'Summoner',
  '#b45106',
  ['Venomancer', 'Nature\'s_prophet', 'Meepo', 'Shadow_shaman', 'Lycan', 'Lone_druid'],
  new Bonus(
    2,
    2,
    [['20'], ['40']],
    tag`Allied summoned units gain +${0}% damage.`
  ),
)
const Swordsman = new Alliance(
  'Swordsman',
  '#c09006',
  ['Phantom_assassin', 'Juggernaut', 'Kunkka', 'Ember_spirit', 'Sven', 'Pangolier', 'Wraith_king'],
  new Bonus(
    3,
    2,
    [['10'], ['25']],
    tag`Each Swordsman unit gains +${0}% bonus damage for each enemy unit adjacent to it.`
  ),
)
const Troll = new Alliance(
  'Troll',
  '#795133',
  ['Batrider', 'Dazzle', 'Shadow_shaman', 'Troll_warlord'],
  new Bonus(
    2,
    2,
    [['35', '15'], ['65', '30']],
    tag`All Troll units gain +${0} Attack Speed and other Allies gain +${1} Attack Speed.`
  ),
)
const Vigilant = new Alliance(
  'Vigilant',
  '#588dc1',
  ['Drow_ranger', 'Luna', 'Windranger', 'Templar_assassin', 'Mirana'],
  new Bonus(
    2,
    2,
    [['Allied Vigilant units', '40'], ['All allied units', '50']],
    tag`Whenever an enemy unit casts a spell, ${0} target that enemy if it is within their range. The damage of the next attack against this target is increased by ${1}%.`
  ),
)
const Void = new Alliance(
  'Void',
  '#655965',
  ['Spectre', 'Templar_assassin', 'Void_spirit', 'Faceless_void'],
  new Bonus(
    3,
    1,
    [[]],
    tag`All allied heroes have a 40% chance to deal an additional 4% of the target's Max Health as Pure Damage. This effect can only occur once every 0.5 seconds.`
  ),
)
const Warrior = new Alliance(
  'Warrior',
  '#0c5193',
  ['Tusk', 'Slardar', 'Kunkka', 'Pudge', 'Earth_spirit', 'Tidehunter', 'Troll_warlord'],
  new Bonus(
    3,
    2,
    [['10'], ['25']],
    tag`All Warriors gain +${0} Armor.`
  ),
)

export const Alliances: Map<string, Alliance> = new Map([
  Assassin,
  Brawny,
  Brute,
  Champion,
  Demon,
  Dragon,
  Fallen,
  Healer,
  Heartless,
  Human,
  Hunter,
  Knight,
  Mage,
  Magus,
  Poisoner,
  Rogue,
  Savage,
  Scaled,
  Shaman,
  Spirit,
  Summoner,
  Swordsman,
  Troll,
  Vigilant,
  Void,
  Warrior,
].map(alliance => [alliance.id, alliance]))
