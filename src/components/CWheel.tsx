import React, { useRef, useEffect } from 'react'
import * as d3 from 'd3'
import { style } from 'typestyle'
import { Alliances } from '../data/Alliances'
import { Heros } from '../data/Heros'
import { useSelector } from 'react-redux'
import { AppState } from '../store/store'
import { Alliance } from '../models/Alliance'
import { Hero } from '../models/Hero'
import Constant from '../utils/Constant'
import CAlliance from './CAlliance'
import CLogo from './CLogo'
import { divideSector } from '../utils/Shape'

const svgClassName = style({
  flex: 1,
  width: '100%',
  height: '100%',
  padding: 16,
  boxSizing: 'border-box',
})

export default function() {
  const ref = useRef<SVGSVGElement>(null)
  const alliances = Array.from(Alliances.values())
  const heros = Array.from(Heros.values())
  const wheelRotateOffset = useSelector((state: AppState) => state.wheelRotateOffset)

  useEffect(
    () => {
      if (ref.current) {
        const defs = d3.select(ref.current).select('defs').attr('display', 'none')
        // append all hero images in defs
        defs.append('g')
          .selectAll('image')
          .data(heros)
          .join('image')
          .attr('id', (hero: Hero) => hero.id)
          .attr('href', (hero: Hero) => hero.image)
          .attr('width', Constant.HERO_ICON)
          .attr('height', Constant.HERO_ICON)
        // append all alliance images in defs
        defs.append('g')
          .selectAll('image')
          .data(alliances)
          .join('image')
          .attr('id', (alliance: Alliance) => alliance.id)
          .attr('href', (alliance: Alliance) => alliance.image)
          .attr('width', Constant.ALLIANCE_ICON)
          .attr('height', Constant.ALLIANCE_ICON)
      }
    },
    [ref, alliances, heros]
  )

  return (
    <svg className={svgClassName} viewBox={`${-Constant.VIEW_BOX / 2} ${-Constant.VIEW_BOX / 2} ${Constant.VIEW_BOX} ${Constant.VIEW_BOX}`} ref={ref}>
      <defs />
      <CLogo />
      <g className='alliances'>
        {alliances.map((alliance, index) =>
          <CAlliance
            key={alliance.id}
            alliance={alliance}
            sector={divideSector(
              Constant.FULL_CIRCLE,
              Constant.ALLIANCE_COUNT,
              (index + wheelRotateOffset) % Constant.ALLIANCE_COUNT
            )} />
          )}
      </g>
    </svg>
  )
}
