import {enableMapSet} from 'immer'
import { configureStore } from '@reduxjs/toolkit'
import { createLogger } from 'redux-logger'
import rootReducer from './reducers'

enableMapSet()

const middleware = []

if (process.env.NODE_ENV === 'development') {
  middleware.push(createLogger({ duration: true, diff: true, collapsed: true }));
}

const store = configureStore({
  reducer: rootReducer,
  middleware,
});

export type AppState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
export default store
