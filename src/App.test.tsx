import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('renders SVGs', () => {
  const { container } = render(<App />);
  expect(container.getElementsByTagName('svg')).toHaveLength(11);
});
