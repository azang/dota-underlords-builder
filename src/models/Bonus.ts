import { AssertionError } from 'assert'

export class Bonus {
  i: number
  j: number
  effects: string[][]
  template: (...values: string[]) => string

  constructor(i: number, j: number, effects: string[][], template: (...values: string[]) => string) {
    if (effects.length !== j)
      throw new AssertionError({message: 'effects.length does not match the bonus levels!'})
    this.i = i
    this.j = j
    this.effects = effects
    this.template = template
  }

  public Effect(level: number): string {
    return this.template(...this.effects[level - 1])
  }

  public Size(): number {
    return this.i * this.j
  }

  public Position(index: number): {i: number, j: number} {
    // given the 0-indexed index of the bonus cell
    // return the i and j coordinates pair of the cell in bonus grid
    return {i: index % this.i, j: Math.floor(index / this.i)}
  }

  public Level(count: number): number {
    // given the count of heros
    // return the bonus level
    if (count <= 0) return 0
    return Math.min(Math.floor(count / this.i), this.j)
  }

  public Difference(count: number): {diff_i: number, diff_j: number} {
    // given the count of heros, return
    // - diff_i: number of cells left to activate the next bonus level
    // - diff_j: number of levels left to fully activate the bonus
    if (count <= 0) return {diff_i: this.i, diff_j: this.j}
    const bounded = Math.min(count, this.Size())
    const position = this.Position(bounded - 1)
    return {diff_i: this.i - position.i - 1, diff_j: this.j - position.j - 1}
  }
}
