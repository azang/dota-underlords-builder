import React from 'react'
import { style } from 'typestyle'
import CDeck from './CDeck'
import CFooter from './CFooter'
import CGrid from './CGrid'
import CHotkeyMenu from './CHotkeyMenu'
import CPlayerLevel from './CPlayerLevel'

const panelClassName = style({
  flex: 'none',
  display: 'flex',
  flexDirection: 'column',
  width: 480,
  height: '100%',
  borderLeft: '1px solid lightgray',
  color: 'gray',
})

export default function() {
  return (
    <div className={panelClassName}>
      <CPlayerLevel />
      <CDeck />
      <CGrid />
      <CHotkeyMenu />
      <CFooter />
    </div>
  )
}