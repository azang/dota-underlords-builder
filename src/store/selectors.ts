import { AppState } from './store'
import { Alliance } from '../models/Alliance'
import { createSelector } from '@reduxjs/toolkit'
import { Hero } from '../models/Hero'
import { Heros } from '../data/Heros'

export const allianceHoveredSelector = (alliance: Alliance) => createSelector(
  (state: AppState) => state.hoveredAlliance,
  (hoveredAlliance: Alliance | null) =>
    hoveredAlliance !== null && hoveredAlliance.id === alliance.id
)

export const allianceHasHoveredHeroSelector = (alliance: Alliance) => createSelector(
  (state: AppState) => state.hoveredHero,
  (hoveredHero: Hero | null) =>
    hoveredHero !== null && alliance.heros.indexOf(hoveredHero.id) !== -1
)

export const allianceIntersectsHoveredAllianceSelector = (alliance: Alliance) => createSelector(
  (state: AppState) => state.hoveredAlliance,
  (hoveredAlliance: Alliance | null) =>
    hoveredAlliance !== null && hoveredAlliance.id !== alliance.id && alliance.heros.some(
      hero => hoveredAlliance.heros.includes(hero))
)

export const allianceSelectedHerosCountSelector = (alliance: Alliance) => createSelector(
  (state: AppState) => state.selectedHeros,
  (selectedHeros: Set<Hero>) => alliance.heros.reduce<number>(
    (count, hero) => count + (selectedHeros.has(Heros.get(hero)!) ? 1 : 0), 0)
)

export const heroHoveredSelector = (hero: Hero) => createSelector(
  (state: AppState) => state.hoveredHero,
  (hoveredHero: Hero | null) =>
    hoveredHero !== null && hoveredHero.id === hero.id
)

export const heroInHoveredAllianceSelector = (hero: Hero) => createSelector(
  (state: AppState) => state.hoveredAlliance,
  (hoveredAlliance: Alliance | null) =>
    hoveredAlliance !== null && hero.alliances.indexOf(hoveredAlliance.id) !== -1
)

export const heroSelectedSelector = (hero: Hero) => createSelector(
  (state: AppState) => state.selectedHeros,
  (selectedHeros: Set<Hero>) => selectedHeros.has(hero)
)

export const hoveredHeroSelectedSelector = createSelector(
  (state: AppState) => state.hoveredHero,
  (state: AppState) => state.selectedHeros,
  (hoveredHero: Hero | null, selectedHeros: Set<Hero>) =>
    hoveredHero !== null && selectedHeros.has(hoveredHero)
)

export const heroHiddenSelector = (hero: Hero) => createSelector(
  (state: AppState) => state.hideTier1,
  (state: AppState) => state.hideTier2,
  (state: AppState) => state.hideTier3,
  (state: AppState) => state.hideTier4,
  (state: AppState) => state.hideTier5,
  (...args: boolean[]) => args[hero.tier - 1]
)

export const selectedHerosSelector = createSelector(
  (state: AppState) => state.selectedHeros,
  (selectedHeros: Set<Hero>) =>
    Array.from(selectedHeros.values()).sort((a, b) => {
      if (a.tier !== b.tier) return a.tier - b.tier
      else return a.name < b.name ? -1 : 1
    })
)

export const selectedHeroAllianceGridSelector = createSelector(
  selectedHerosSelector,
  (selectedHeros: Hero[]) => {
    const selectedAlliances: Set<Alliance> = new Set()
    selectedHeros.forEach(hero => hero.Alliances().forEach(alliance =>
      selectedAlliances.add(alliance)
    ))

    const grid: {alliance: Alliance, heros: (Hero | null)[], count: number}[] = []
    Array.from(selectedAlliances.values()).forEach(alliance => {
      const row = {alliance, heros: [], count: 0} as {
        alliance: Alliance,
        heros: (Hero | null)[],
        count: number
      }
      selectedHeros.forEach(hero => {
        if (hero.HasAlliance(alliance)) {
          row.heros.push(hero)
          row.count += 1
        }
        else row.heros.push(null)
      })
      grid.push(row)
    })
    return grid.sort((rowA, rowB) => {
      const bonusA = rowA.alliance.bonus
      const bonusB = rowB.alliance.bonus
      const countA = rowA.count
      const countB = rowB.count
      const levelA = bonusA.Level(countA)
      const levelB = bonusB.Level(countB)
      const diffA = bonusA.Difference(countA)
      const diffB = bonusB.Difference(countB)
      // when either one is activated:
      // - the activated one comes first
      if (levelA !== 0 && levelB === 0) return -1
      if (levelA === 0 && levelB !== 0) return 1
      // when both alliance bonuses are (not) activated:
      // 1. the one closer to being activated for next level comes first
      // 2. when tie above, the one closer to being fully activated comes first
      // 3. when tie above, name as tie breaker
      if (diffA.diff_i !== diffB.diff_i) return diffA.diff_i - diffB.diff_i
      else if (diffA.diff_j !== diffB.diff_j) return diffA.diff_j - diffB.diff_j
      else return rowA.alliance.name < rowB.alliance.name ? -1 : 1
    })
  }
)
