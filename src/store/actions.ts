import { createAction } from '@reduxjs/toolkit'

export const setHoveredAlliance = createAction<string>('SET_HOVERED_ALLIANCE')
export const setHoveredHero = createAction<string>('SET_HOVERED_HERO')
export const toggleHeroSelection = createAction<string>('TOGGLE_HERO_SELECTION')
export const togglePlayerLevel = createAction<1|-1>('TOGGLE_PLAYER_LEVEL')
// HOTKEYs
export const toggleTier = createAction<number>('TOGGLE_TIER')
export const lockUnlockHeroSelection = createAction('LOCK_UNLOCK_HERO_SELECTION')
export const clearHeroSelection = createAction('CLEAR_HERO_SELECTION')
export const rotateWheel = createAction<1|-1>('ROTATE_WHEEL')
export const toggleHotkeyMenu = createAction('TOGGLE_HOTKEY_MENU')