import { Hero } from '../models/Hero'

const AntiMage = new Hero('Anti-mage', 1, ['Rogue', 'Hunter'])
const Batrider = new Hero('Batrider', 1, ['Troll', 'Knight'])
const BountyHunter = new Hero('Bounty_hunter', 1, ['Rogue', 'Assassin'])
const CrystalMaiden = new Hero('Crystal_maiden', 1, ['Human', 'Mage'])
const Dazzle = new Hero('Dazzle', 1, ['Troll', 'Healer', 'Poisoner'])
const DrowRanger = new Hero('Drow_ranger', 1, ['Heartless', 'Vigilant', 'Hunter'])
const Enchantress = new Hero('Enchantress', 1, ['Shaman', 'Healer'])
const Lich = new Hero('Lich', 1, ['Fallen', 'Mage'])
const Magnus = new Hero('Magnus', 1, ['Savage', 'Shaman'])
const PhantomAssassin = new Hero('Phantom_assassin', 1, ['Assassin', 'Swordsman'])
const ShadowDemon = new Hero('Shadow_demon', 1, ['Heartless', 'Demon'])
const Slardar = new Hero('Slardar', 1, ['Scaled', 'Warrior'])
const Snapfire = new Hero('Snapfire', 1, ['Brawny', 'Dragon'])
const Tusk = new Hero('Tusk', 1, ['Savage', 'Warrior'])
const VengefulSpirit = new Hero('Vengeful_spirit', 1, ['Fallen', 'Heartless'])
const Venomancer = new Hero('Venomancer', 1, ['Scaled', 'Summoner', 'Poisoner'])
const Bristleback = new Hero('Bristleback', 2, ['Brawny', 'Savage'])
const ChaosKnight = new Hero('Chaos_knight', 2, ['Demon', 'Knight'])
const EarthSpirit = new Hero('Earth_spirit', 2, ['Spirit', 'Warrior'])
const Juggernaut = new Hero('Juggernaut', 2, ['Brawny', 'Swordsman'])
const Kunkka = new Hero('Kunkka', 2, ['Human', 'Warrior', 'Swordsman'])
const LegionCommander = new Hero('Legion_commander', 2, ['Human', 'Champion'])
const Luna = new Hero('Luna', 2, ['Vigilant', 'Knight'])
const Meepo = new Hero('Meepo', 2, ['Rogue', 'Summoner'])
const NaturesProphet = new Hero('Nature\'s_prophet', 2, ['Shaman', 'Summoner'])
const Pudge = new Hero('Pudge', 2, ['Heartless', 'Warrior'])
const QueenOfPain = new Hero('Queen_of_pain', 2, ['Demon', 'Assassin', 'Poisoner'])
const SpiritBreaker = new Hero('Spirit_breaker', 2, ['Savage', 'Brute'])
const StormSpirit = new Hero('Storm_spirit', 2, ['Spirit', 'Mage'])
const Windranger = new Hero('Windranger', 2, ['Vigilant', 'Hunter'])
const Abaddon = new Hero('Abaddon', 3, ['Fallen', 'Knight'])
const Alchemist = new Hero('Alchemist', 3, ['Brute', 'Rogue', 'Poisoner'])
const Beastmaster = new Hero('Beastmaster', 3, ['Brawny', 'Hunter', 'Shaman'])
const EmberSpirit = new Hero('Ember_spirit', 3, ['Spirit', 'Assassin', 'Swordsman'])
const Lifestealer = new Hero('Lifestealer', 3, ['Heartless', 'Brute'])
const Lycan = new Hero('Lycan', 3, ['Human', 'Savage', 'Summoner'])
const Omniknight = new Hero('Omniknight', 3, ['Human', 'Knight', 'Healer'])
const Puck = new Hero('Puck', 3, ['Dragon', 'Mage'])
const ShadowShaman = new Hero('Shadow_shaman', 3, ['Troll', 'Summoner'])
const Slark = new Hero('Slark', 3, ['Scaled', 'Assassin'])
const Spectre = new Hero('Spectre', 3, ['Void', 'Demon'])
const Terrorblade = new Hero('Terrorblade', 3, ['Demon', 'Hunter', 'Fallen'])
const TreantProtector = new Hero('Treant_protector', 3, ['Shaman', 'Healer'])
const DeathProphet = new Hero('Death_prophet', 4, ['Fallen', 'Heartless'])
const Doom = new Hero('Doom', 4, ['Demon', 'Brute'])
const Lina = new Hero('Lina', 4, ['Human', 'Mage'])
const LoneDruid = new Hero('Lone_druid', 4, ['Savage', 'Shaman', 'Summoner'])
const Mirana = new Hero('Mirana', 4, ['Vigilant', 'Hunter'])
const Pangolier = new Hero('Pangolier', 4, ['Savage', 'Swordsman'])
const Rubick = new Hero('Rubick', 4, ['Mage', 'Magus'])
const Sven = new Hero('Sven', 4, ['Rogue', 'Knight', 'Swordsman'])
const TemplarAssassin = new Hero('Templar_assassin', 4, ['Vigilant', 'Void', 'Assassin'])
const Tidehunter = new Hero('Tidehunter', 4, ['Scaled', 'Warrior'])
const Viper = new Hero('Viper', 4, ['Dragon', 'Poisoner'])
const VoidSpirit = new Hero('Void_spirit', 4, ['Void', 'Spirit'])
const Axe = new Hero('Axe', 5, ['Brawny', 'Brute'])
const DragonKnight = new Hero('Dragon_knight', 5, ['Human', 'Dragon', 'Knight'])
const FacelessVoid = new Hero('Faceless_void', 5, ['Void', 'Assassin'])
const KeeperOfTheLight = new Hero('Keeper_of_the_light', 5, ['Human', 'Mage'])
const Medusa = new Hero('Medusa', 5, ['Scaled', 'Hunter'])
const TrollWarlord = new Hero('Troll_warlord', 5, ['Troll', 'Warrior'])
const WraithKing = new Hero('Wraith_king', 5, ['Fallen', 'Swordsman'])

export const Heros: Map<string, Hero> = new Map([
  Abaddon,
  Alchemist,
  AntiMage,
  Axe,
  Batrider,
  Beastmaster,
  BountyHunter,
  Bristleback,
  ChaosKnight,
  CrystalMaiden,
  Dazzle,
  DeathProphet,
  Doom,
  DragonKnight,
  DrowRanger,
  EarthSpirit,
  EmberSpirit,
  Enchantress,
  FacelessVoid,
  Juggernaut,
  KeeperOfTheLight,
  Kunkka,
  LegionCommander,
  Lich,
  Lifestealer,
  Lina,
  LoneDruid,
  Luna,
  Lycan,
  Magnus,
  Medusa,
  Meepo,
  Mirana,
  NaturesProphet,
  Omniknight,
  Pangolier,
  PhantomAssassin,
  Puck,
  Pudge,
  QueenOfPain,
  Rubick,
  ShadowDemon,
  ShadowShaman,
  Slardar,
  Slark,
  Snapfire,
  Spectre,
  SpiritBreaker,
  StormSpirit,
  Sven,
  TemplarAssassin,
  Terrorblade,
  Tidehunter,
  TreantProtector,
  TrollWarlord,
  Tusk,
  VengefulSpirit,
  Venomancer,
  Viper,
  VoidSpirit,
  Windranger,
  WraithKing,
].map(hero => [hero.id, hero]))
