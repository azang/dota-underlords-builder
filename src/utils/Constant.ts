import { Alliances } from '../data/Alliances'
import { color } from 'csx'
import { Ring, Sector } from './Shape'

export default {
  VIEW_BOX: 2048,
  ALLIANCE_COUNT: Alliances.size,
  // GRID
  GRID_WIDTH: 16,
  GRID_COUNT: 64,
  PAD_GRID: 32,
  TIER_RINGS: [
    { innerGrid: 60, outerGrid: 64 } as Ring,
    { innerGrid: 56, outerGrid: 60 } as Ring,
    { innerGrid: 52, outerGrid: 56 } as Ring,
    { innerGrid: 48, outerGrid: 52 } as Ring,
    { innerGrid: 44, outerGrid: 48 } as Ring,
  ],
  ALLIANCE_RING: { innerGrid: 36, outerGrid: 44 } as Ring,
  BONUS_RINGS: [
    [
      { innerGrid: 32, outerGrid: 34 } as Ring,
    ],
    [
      { innerGrid: 33, outerGrid: 35 } as Ring,
      { innerGrid: 31, outerGrid: 33 } as Ring,
    ],
    [
      { innerGrid: 34, outerGrid: 36 } as Ring,
      { innerGrid: 32, outerGrid: 34 } as Ring,
      { innerGrid: 30, outerGrid: 32 } as Ring,
    ],
  ],
  FULL_CIRCLE: { startAngle: 0, endAngle: 2 * Math.PI } as Sector,
  // ICON
  LOGO_ICON: 956,
  HERO_ICON: 48,
  ALLIANCE_ICON: 64,
  // COLOR
  TIER_COLORS: [
    color('#808080'),
    color('#556B2f'),
    color('#4169e1'),
    color('#9400d3'),
    color('#ffa500'),
  ],
  // OTHER
  SHOP_ODDS: [
    [80, 20,  0,  0,  0],
    [70, 30,  0,  0,  0],
    [55, 35, 10,  0,  0],
    [45, 40, 15,  0,  0],
    [35, 40, 25,  0,  0],
    [25, 35, 35,  5,  0],
    [20, 30, 40, 10,  0],
    [18, 24, 35, 20,  3],
    [15, 21, 30, 28,  6],
    [12, 18, 28, 32, 10],
  ],
  // VERSION
  UPDATE_DATE: 'September 24th, 2020'
}