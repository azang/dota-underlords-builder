import React from 'react'
import { useSelector } from 'react-redux'
import { style } from 'typestyle'
import { selectedHerosSelector } from '../store/selectors'
import Constant from '../utils/Constant'

const deckClassName = style({
  flex: 'none',
  display: 'flex',
  flexWrap: 'wrap',
  borderBottom: '1px solid lightgray',
  padding: 8,
  $nest: {
    '.slot': {
      width: '20%',
      position: 'relative',
      $nest: {
        '&:before': {
          content: `""`,
          display: 'block',
          paddingTop: '100%',
        },
        '.hero': {
          position: 'absolute',
          top: 0,
          bottom: 0,
          left: 0,
          right: 0,
          padding: 4,
          boxSizing: 'border-box',
          width: '100%',
          height: '100%',
        },
      }
    }
  }
})

export default function() {
  const selectedHeros = useSelector(selectedHerosSelector)

  return (
    <div className={deckClassName}>
      {[0, 1, 2, 3, 4, 5, 6, 7, 8, 9].map(index => {
        const hero = selectedHeros[index]
        return (
          <div key={index} className='slot'>
            <svg viewBox='-32 -32 64 64' className='hero'>
              <rect
                x='-30'
                y='-30'
                width='60'
                height='60'
                rx='8'
                stroke={hero !== undefined ? `${Constant.TIER_COLORS[hero.tier - 1].darken(0.25).toHexString()}` : '#ccc'}
                strokeWidth={hero !== undefined ? '2' : '1'}
                strokeDasharray={hero !== undefined ? '' : '8 2 4 6'}
                fill={hero !== undefined ? `${Constant.TIER_COLORS[hero.tier - 1].tint(0.25).toHexString()}` : 'transparent'} />
              {hero && <image x='-16' y='-16' width='32' height='32' href={hero.image} />}
            </svg>
          </div>
        )}
      )}
    </div>
  )
}