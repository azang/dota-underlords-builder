import React, { useRef, useEffect, useCallback } from 'react'
import * as d3 from 'd3'
import { Alliance } from '../models/Alliance'
import Constant from '../utils/Constant'
import { Arc, divideSector, getArc, middleAngle, Ring, Sector } from '../utils/Shape'
import { useDispatch, useSelector } from 'react-redux'
import {
  allianceHoveredSelector,
  allianceHasHoveredHeroSelector,
  allianceIntersectsHoveredAllianceSelector,
  allianceSelectedHerosCountSelector,
  hoveredHeroSelectedSelector
} from '../store/selectors'
import { setHoveredAlliance } from '../store/actions'
import CHero from './CHero'
import { AppState } from '../store/store'

type CAllianceProps = {
  alliance: Alliance
  sector: Sector
}

export default function({alliance, sector}: CAllianceProps) {
  const ref = useRef<SVGGElement>(null)
  const dispatch = useDispatch()
  const allianceColor = alliance.color
  const locked = useSelector((state: AppState) => state.heroSelectionLocked)
  const allianceHovered = useSelector(allianceHoveredSelector(alliance))
  const allianceHasHoveredHero = useSelector(allianceHasHoveredHeroSelector(alliance))
  const allianceIntersectsHoveredAlliance = useSelector(allianceIntersectsHoveredAllianceSelector(alliance))
  const allianceSelectedHerosCount = useSelector(allianceSelectedHerosCountSelector(alliance))
  const hoveredHeroSelected = useSelector(hoveredHeroSelectedSelector)
  const highlight = allianceHovered || allianceHasHoveredHero || allianceIntersectsHoveredAlliance
  const easeOn = useCallback(
    (selection: any) => {
      selection
        .transition()
        .duration(800)
        .attr('stroke-width', 4)
        .attr('fill', allianceColor.toHexString())
        .ease(d3.easeQuadOut)
        .transition()
        .duration(800)
        .attr('stroke-width', 2)
        .attr('fill', allianceColor.tint(0.5).toHexString())
        .ease(d3.easeQuadIn)
        .on('end', () => easeOn(selection))
    },
    [allianceColor]
  )
  const easeOff = useCallback(
    (selection: any) => {
      selection
        .transition()
        .duration(800)
        .attr('stroke-width', 2)
        .attr('fill', 'white')
        .ease(d3.easeQuadOut)
        .transition()
        .duration(800)
        .attr('stroke-width', 4)
        .attr('fill', allianceColor.tint(0.5).toHexString())
        .ease(d3.easeQuadIn)
        .on('end', () => easeOff(selection))
    },
    [allianceColor]
  )
  // draw the alliance components
  useEffect(
    () => {
      const arc = getArc({ring: Constant.ALLIANCE_RING, sector, padding: 4} as Arc)
      const rotate = 180 * middleAngle(sector) / Math.PI
      if (ref.current) {
        const group = d3.select(ref.current)
        group.select('path.alliance-arc')
          .attr('d', arc.path)
          .attr('pointer-events', locked ? 'none' : 'unset')
          .on('mouseenter', () => dispatch(setHoveredAlliance(alliance.id)))
          .on('mouseleave', () => dispatch(setHoveredAlliance('')))
        group.select('use.alliance-icon')
          .attr('href', `#${alliance.id}`)
          .attr('x', arc.center.x - Constant.ALLIANCE_ICON / 2)
          .attr('y', arc.center.y - Constant.ALLIANCE_ICON / 2)
          .attr('transform', `rotate(${rotate}, ${arc.center.x}, ${arc.center.y})`)
          .attr('pointer-events', 'none')
        group.select('g.alliance-bonus')
          .selectAll('path')
          .data(d3.range(0, alliance.bonus.Size(), 1)
            .map((index: number) => alliance.bonus.Position(index))
            .map((pos: {i: number, j: number}) => ({
              ring: Constant.BONUS_RINGS[alliance.bonus.j - 1][pos.j],
              sector: divideSector(sector, alliance.bonus.i, pos.i),
            })))
          .join('path')
          .attr('d', (cell: {ring: Ring, sector: Sector}) => getArc({...cell, padding: 4}).path)
          .attr('pointer-events', 'none')
      }
    },
    [ref, alliance, sector, locked, dispatch]
  )
  // highlight the alliance arc
  useEffect(
    () => {
      if (ref.current) {
        d3.select(ref.current)
          .select('path.alliance-arc')
          .transition()
          .duration(200)
          .attr('stroke', '#66ffff')
          .attr('stroke-width', 2)
          .attr('fill', highlight ? '#66ffff' : 'transparent')
      }
    },
    [ref, highlight]
  )
  // fill bonus cells
  useEffect(
    () => {
      if (ref.current) {
        const bonus = d3.select(ref.current).select('g.alliance-bonus')
        bonus.selectAll('path')
          .attr('stroke', '#333')
          .attr('class', (_: {ring: Ring, sector: Sector}, index: number) => {
            if (index < allianceSelectedHerosCount - 1)       // [first, lastSelected)
              return 'on'
            if (index === allianceSelectedHerosCount - 1)     // lastSelected
              return allianceHasHoveredHero && hoveredHeroSelected ? 'ease-off' : 'on'
            if (index > allianceSelectedHerosCount)           // (next, last]
              return 'off'
            if (index === allianceSelectedHerosCount)         // next
              return allianceHasHoveredHero && !hoveredHeroSelected ? 'ease-on' : 'off'
          })
        bonus.selectAll('path.on')
          .transition()
          .duration(200)
          .attr('stroke-width', 4)
          .attr('fill', allianceColor.toHexString())
        bonus.selectAll('path.off')
          .transition()
          .duration(200)
          .attr('stroke-width', 2)
          .attr('fill', 'white')
        easeOn(bonus.selectAll('path.ease-on'))
        easeOff(bonus.selectAll('path.ease-off'))
      }
    },
    [ref, allianceSelectedHerosCount, allianceHasHoveredHero, hoveredHeroSelected, allianceColor, easeOn, easeOff]
  )

  return (
    <g className='alliance' ref={ref}>
      <g className='alliance-heros'>
        {[1, 2, 3, 4, 5].map(tier =>
          <g key={tier} className={`tier-${tier}-heros`}>
            {alliance.Heros(tier).map((hero, index, heros) =>
              <CHero key={hero.id} hero={hero} sector={divideSector(sector, heros.length, index)} />)}
          </g>
        )}
      </g>
      <path className='alliance-arc' />
      <use className='alliance-icon' />
      <g className='alliance-bonus' />
    </g>
  )
}