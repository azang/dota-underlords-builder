import React, { useEffect } from 'react'
import CSidePanel from './components/CSidePanel'
import CWheel from './components/CWheel'
import store from './store/store'
import { Provider } from 'react-redux'
import {
  toggleTier,
  lockUnlockHeroSelection,
  clearHeroSelection,
  rotateWheel,
  toggleHotkeyMenu
} from './store/actions'

const keydownHandler = (event: KeyboardEvent) => {
  switch (event.key.toUpperCase()) {
    case '1':
    case '2':
    case '3':
    case '4':
    case '5': {
      store.dispatch(toggleTier(Number.parseInt(event.key)))
      break
    }
    case 'Q': {
      store.dispatch(lockUnlockHeroSelection())
      break
    }
    case 'W': {
      store.dispatch(clearHeroSelection())
      break
    }
    case 'E': {
      store.dispatch(rotateWheel(-1))
      break
    }
    case 'R': {
      store.dispatch(rotateWheel(1))
      break
    }
    case ' ': {
      store.dispatch(toggleHotkeyMenu())
      break
    }
    default: break
  }
}

export default function() {
  useEffect(() => {
    window.addEventListener('keydown', keydownHandler)
    return () => window.removeEventListener('keydown', keydownHandler)
  })

  return (
    <Provider store={store}>
      <CWheel />
      <CSidePanel />
    </Provider>
  )
}
