import * as d3 from 'd3'
import Constant from './Constant'

export type Ring = {
  innerGrid: number
  outerGrid: number
}

export type Sector = {
  startAngle: number
  endAngle: number
}


export type Arc = {
  ring: Ring
  sector: Sector
  padding?: number
}

export const getRadius = (ring: Ring, padding?: number): {inner: number, outer: number} => {
  return {
    inner: ring.innerGrid * Constant.GRID_WIDTH + (padding ?? 0),
    outer: ring.outerGrid * Constant.GRID_WIDTH - (padding ?? 0),
  }
}

export const middleAngle = (sector: Sector): number => {
  return (sector.startAngle + sector.endAngle) / 2
}

export const divideSector = (sector: Sector, count: number, index: number): Sector => {
  const theta = (sector.endAngle - sector.startAngle) / count
  return {
    startAngle: sector.startAngle + index * theta,
    endAngle: sector.startAngle + (index + 1) * theta,
  }
}

export const getArc = (arc: Arc) => {
  const generator = d3.arc()
    .innerRadius((arc: Arc) => getRadius(arc.ring, arc.padding).inner)
    .outerRadius((arc: Arc) => getRadius(arc.ring, arc.padding).outer)
    .startAngle((arc: Arc) => arc.sector.startAngle)
    .endAngle((arc: Arc) => arc.sector.endAngle)
    .padRadius(Constant.PAD_GRID * Constant.GRID_WIDTH)
    .padAngle(Math.PI / 180)
    .cornerRadius(4)
  const path: string = generator(arc)
  const [x, y] = generator.centroid(arc)

  return { path, center: { x, y } }
}