import { Alliance } from './Alliance'
import { Alliances } from '../data/Alliances'

export class Hero {
  readonly id: string
  readonly name: string
  readonly tier: number
  readonly image: string
  readonly alliances: string[]

  constructor(id: string, tier: number, alliances: string[]) {
    this.id = id
    this.name = id.replace(/_/g, ' ').toUpperCase()
    this.tier = tier
    this.image = `heros/tier-${tier}/${id}_mini_icon.png`
    this.alliances = alliances
  }

  public Alliances(): Alliance[] {
    return this.alliances.map(alliance => Alliances.get(alliance)!)
  }

  public HasAlliance(alliance: Alliance): boolean {
    return this.alliances.includes(alliance.id)
  }
}