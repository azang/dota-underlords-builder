import React, { useEffect, useRef } from 'react'
import * as d3 from 'd3'
import { style } from 'typestyle'
import { useSelector } from 'react-redux'
import { selectedHeroAllianceGridSelector } from '../store/selectors'
import { Hero } from '../models/Hero'
import { Alliance } from '../models/Alliance'
import { NestedCSSProperties } from 'typestyle/lib/types'
import Constant from '../utils/Constant'

const beforeStyle: NestedCSSProperties = {
  content: `""`,
  display: 'block',
  paddingTop: '100%',
}
const iconStyle: NestedCSSProperties = {
  display: 'flex',
  position: 'absolute',
  top: 0,
  bottom: 0,
  left: 0,
  right: 0,
  margin: 2,
}
const gridClassName = style({
  flex: 1,
  display: 'flex',
  flexDirection: 'column',
  padding: 8,
  overflowY: 'auto',
  $nest: {
    '.row': {
      display: 'flex',
      padding: '2px 0',
      borderTop: '1px solid lightgray',
      $nest: {
        '&:last-of-type': {
          borderBottom: '1px solid lightgray',
        },
        '.alliance-slot': {
          width: '8%',
          position: 'relative',
          $nest: {
            '&:before': {
              ...beforeStyle
            },
            '.icon': {
              ...iconStyle,
              $nest: {
                'img': {
                  width: '100%',
                  height: '100%',
                }
              }
            },
          }
        },
        '.hero-slot': {
          width: '8%',
          position: 'relative',
          $nest: {
            '&:before': {
              ...beforeStyle
            },
            '.icon': {
              ...iconStyle,
              border: '2px solid',
              borderRadius: 4,
              $nest: {
                '&.tier-1': {
                  background: Constant.TIER_COLORS[0].tint(0.25).toHexString(),
                  borderColor: Constant.TIER_COLORS[0].darken(0.25).toHexString(),
                },
                '&.tier-2': {
                  background: Constant.TIER_COLORS[1].tint(0.25).toHexString(),
                  borderColor: Constant.TIER_COLORS[1].darken(0.25).toHexString(),
                },
                '&.tier-3': {
                  background: Constant.TIER_COLORS[2].tint(0.25).toHexString(),
                  borderColor: Constant.TIER_COLORS[2].darken(0.25).toHexString(),
                },
                '&.tier-4': {
                  background: Constant.TIER_COLORS[3].tint(0.25).toHexString(),
                  borderColor: Constant.TIER_COLORS[3].darken(0.25).toHexString(),
                },
                '&.tier-5': {
                  background: Constant.TIER_COLORS[4].tint(0.25).toHexString(),
                  borderColor: Constant.TIER_COLORS[4].darken(0.25).toHexString(),
                },
                'img': {
                  width: '80%',
                  height: '80%',
                  margin: 'auto',
                }
              }
            },
          }
        }
      }
    }
  }
})

export default function() {
  const ref = useRef<HTMLDivElement>(null)
  const selectedHeroAllianceGrid = useSelector(selectedHeroAllianceGridSelector)

  useEffect(
    () => {
      if (ref.current) {
        d3.select(ref.current)
          .selectAll('div.row')
          .data(selectedHeroAllianceGrid)
          .join(
            (enter: any) => {
              const row = enter.append('div').attr('class', 'row')
              // append a alliance-slot div for the alliance
              row.append('div').attr('class', 'alliance-slot')
                .append('div').attr('class', 'icon')
                .append('img')
                .attr('src', ({alliance}: {alliance: Alliance}) => alliance.image)
                .attr('alt', ({alliance}: {alliance: Alliance}) => alliance.name)
              // append a hero-slot div for each hero of the alliance
              row.selectAll('div.hero-slot')
                .data(({heros}: {heros: (Hero | null)[]}) => heros)
                .join(
                  (enter: any) => {
                    enter.append('div').attr('class', 'hero-slot')
                      .each((datum: Hero | null, index: number, nodes: any[]) => {
                        if(datum)
                          d3.select(nodes[index])
                            .append('div').attr('class', `icon tier-${datum.tier}`)
                            .append('img').attr('src', datum.image).attr('alt', datum.name)
                      })
                  },
                  (update: any) => {
                    update.each((datum: Hero | null, index: number, nodes: any[]) => {
                      if(datum) {
                        d3.select(nodes[index])
                          .select('img').attr('src', datum.image).attr('alt', datum.name)
                      } else {
                        d3.select(nodes[index])
                          .select('div.icon')
                          .remove()
                      }
                    })
                  },
                  (exit: any) => exit.remove()
                )
            },
            (update: any) => {
              // update image in alliance-slot for the alliance
              update.select('div.alliance-slot').select('img')
                .attr('src', ({alliance}: {alliance: Alliance}) => alliance.image)
                .attr('alt', ({alliance}: {alliance: Alliance}) => alliance.name)
              // update image in hero-slot for each hero of the alliance
              update.selectAll('div.hero-slot')
                .data(({heros}: {heros: (Hero | null)[]}) => heros)
                .join(
                  (enter: any) => {
                    enter.append('div').attr('class', 'hero-slot')
                      .each((datum: Hero | null, index: number, nodes: any[]) => {
                        if(datum)
                          d3.select(nodes[index])
                            .append('div').attr('class', `icon tier-${datum.tier}`)
                            .append('img').attr('src', datum.image).attr('alt', datum.name)
                      })
                  },
                  (update: any) => {
                    update.each((datum: Hero | null, index: number, nodes: any[]) => {
                      const node = d3.select(nodes[index])
                      if(datum) {
                        if (node.select('div.icon').empty()) {
                          node.append('div').attr('class', `icon tier-${datum.tier}`)
                            .append('img').attr('src', datum.image).attr('alt', datum.name)
                        } else {
                          node.select('div.icon').attr('class', `icon tier-${datum.tier}`)
                            .select('img').attr('src', datum.image).attr('alt', datum.name)
                        }
                      } else {
                        if (!node.select('div.icon').empty()) {
                          node.select('div.icon').remove()
                        }
                      }
                    })
                  },
                  (exit: any) => exit.remove()
                )
            },
            (exit: any) => exit.remove()
          )
      }
    },
    [ref, selectedHeroAllianceGrid]
  )

  return (
    <div className={gridClassName} ref={ref} />
  )
}
