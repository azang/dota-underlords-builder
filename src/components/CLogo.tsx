import React, { useRef, useEffect, useCallback } from 'react'
import * as d3 from 'd3'
import Constant from '../utils/Constant'
import { useSelector } from 'react-redux'
import { AppState } from '../store/store'

export default function() {
  const ref = useRef<SVGGElement>(null)
  const locked = useSelector((state: AppState) => state.heroSelectionLocked)
  const insertLogo = useCallback(
    () => {
      if (ref.current) {
        const group = d3.select(ref.current)
        group.select('svg').remove()
        d3.xml('dota-underlords-logo.svg')
          .then((logo: XMLDocument) => {
            group.node().appendChild(logo.documentElement)
            group.select('svg')
              .attr('x', -Constant.LOGO_ICON / 2)
              .attr('y', -Constant.LOGO_ICON / 2)
              .attr('width', Constant.LOGO_ICON)
              .attr('height', Constant.LOGO_ICON)
              .attr('pointer-events', 'none')
            group.select('rect')
              .attr('fill', 'none')
              .attr('pointer-events', 'none')
          })
      }
    },
    [ref]
  )

  useEffect(
    () => {
      if (ref.current) {
        const group = d3.select(ref.current)
        group.append('circle')
          .attr('r', Constant.LOGO_ICON / 2)
          .attr('fill', locked ? 'gray' : 'lightgray')
          .attr('pointer-events', 'none')
        insertLogo()
      }
    },
    [ref,locked, insertLogo]
  )

  return (
    <g className='logo' ref={ref} />
  )
}