import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { style } from 'typestyle'
import { togglePlayerLevel } from '../store/actions'
import { AppState } from '../store/store'
import Constant from '../utils/Constant'

const playerLevelClassName = style({
  flex: 'none',
  display: 'flex',
  justifyContent: 'space-around',
  textAlign: 'center',
  borderBottom: '1px solid lightgray',
  padding: 8,
  color: 'gray',
  $nest: {
    '.buttons': {
      display: 'flex',
      alignItems: 'center',
      $nest: {
        '.button': {
          padding: 8,
          border: '1px solid lightgray',
          width: 20,
          height: 20,
          lineHeight: '20px',
          borderRadius: 20,
          $nest: {
            '&:hover': {
              background: 'lightgray',
              color: 'white',
            },
            '&.disabled': {
              color: 'lightgray',
              $nest: {
                '&:hover': {
                  background: 'white',
                  color: 'lightgray',
                },
              }
            }
          },
        },
        '.level': {
          fontSize: 16,
          padding: 8,
          border: '2px solid lightgray',
          width: 30,
          height: 30,
          lineHeight: '30px',
          borderRadius: 30,
          margin: '0 8px',
        }

      }
    },
    '.odds': {
      fontSize: 16,
      width: 60,
      height: 20,
      lineHeight: '20px',
      color: 'white',
      borderRadius: 8,
      $nest: {
        '&.tier-1': { background: Constant.TIER_COLORS[0].toHexString() },
        '&.tier-2': { background: Constant.TIER_COLORS[1].toHexString() },
        '&.tier-3': { background: Constant.TIER_COLORS[2].toHexString() },
        '&.tier-4': { background: Constant.TIER_COLORS[3].toHexString() },
        '&.tier-5': { background: Constant.TIER_COLORS[4].toHexString() },
      }
    }
  }
})

export default function() {
  const dispatch = useDispatch()
  const playerLevel = useSelector((state: AppState) => state.playerLevel)

  return (
    <div className={playerLevelClassName}>
      <div className='buttons'>
        <div
          className={`button ${playerLevel === 1 ? 'disabled': ''}`}
          onClick={() => playerLevel !== 1 && dispatch(togglePlayerLevel(-1))}>
          <i className='fas fa-minus' />
        </div>
        <pre className='level'>{playerLevel}</pre>
        <div
          className={`button ${playerLevel === 10 ? 'disabled': ''}`}
          onClick={() => playerLevel !== 10 && dispatch(togglePlayerLevel(1))}>
          <i className='fas fa-plus' />
        </div>
      </div>
      {Constant.SHOP_ODDS[playerLevel - 1].map((odd, tier) =>
        <pre key={tier} className={`odds tier-${tier + 1}`}>{`${odd}%`}</pre>
      )}
    </div>
  )
}