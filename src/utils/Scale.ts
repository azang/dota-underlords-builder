import * as d3 from 'd3'
import Constant from './Constant'

export const tierColors = d3.scaleOrdinal()
  .domain(d3.range(0, 5, 1))
  .range(Constant.TIER_COLORS)