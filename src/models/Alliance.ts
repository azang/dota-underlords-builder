import { ColorHelper, color } from 'csx'
import { Bonus } from './Bonus'
import { Heros } from '../data/Heros'
import { Hero } from './Hero'

export class Alliance {
  readonly id: string
  readonly name: string
  readonly image: string
  readonly color: ColorHelper
  readonly heros: string[]
  readonly bonus: Bonus

  constructor(id: string, rgb: string, heros: string[], bonus: Bonus) {
    this.id = id
    this.name = id.toUpperCase()
    this.image = `alliances/${id}_icon.png`
    this.color = color(rgb)
    this.heros = heros
    this.bonus = bonus
  }

  public Heros(tier: number): Hero[] {
    const heros = this.heros.map(hero => Heros.get(hero)!)
    if (tier === 0)
      return heros
    return heros.filter(hero => hero.tier === tier)
  }

  public HasHero(hero: Hero): boolean {
    return this.heros.includes(hero.id)
  }
}